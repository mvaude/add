from setuptools import setup
# To use a consistent encoding
from codecs import open
from os import path


def get_description():
    here = path.abspath(path.dirname(__file__))

    # Get the long description from the README file
    with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
        return f.read()


setup(
    name='add',
    version='0.1.0',
    description='A Generic Installer',
    long_description=get_description(),
    url='https://gitlab.com/mvaude/add',
    author='mvaude',
    author_email='maxime.vaude@gmail.com',
    license='MIT',
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='add installer',
    packages=['add'],
    install_requires=['invoke>=0.20.2,<1'],
    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
        'dev': ['tox>=2.7.0,<3'],
    },
    entry_points={
        'console_scripts': ['add = add.main:program.run']
    }
)
