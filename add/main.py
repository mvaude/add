from invoke import Collection, Program
from invoke.config import Config
from add import tasks


class AddConfig(Config):
    prefix = 'add'


program = Program(
    namespace=Collection.from_module(tasks),
    version='0.1.0',
    config_class=AddConfig
)
