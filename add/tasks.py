from invoke import task
import fileinput
import sys
import re

DEST = "/mnt"


@task
def get_uuid(ctx, device):
    """
    Get uuid for specific partition
    """
    return ctx.run("find -L /dev/disk/by-uuid -samefile {0}".format(device))


@task
def packages(ctx, package=None):
    """
    Adding packages
    """
    ctx.run('pacman --noconfirm -Syu {0}'.format(package))


@task
def sound(ctx):
    """
    Adding sound
    """
    packages(ctx, "alsa-utils")


@task
def change_awesome_font(ctx, font="sans", size=10):
    """
    Changing default font for awesome
    """
    fileToSearch = "/usr/share/awesome/themes/default/theme.lua"
    with fileinput.FileInput(fileToSearch, inplace=True) as file:
        for line in file:
            if line.startswith("theme.font"):
                line = "theme.font = \"{0} {1}\"\n".format(font, size)
            sys.stdout.write(line)


@task
def test_connection(ctx):
    """
    Test internet connection
    """
    ctx.run("ping -c 1 www.google.com")


@task
def partition_disk(ctx, device=None):
    """
    Partition Disk for Linux installation
    """
    # destroying mbr or gpt
    ctx.run("sgdisk -Z {0}".format(device))
    # creating new gpt table
    ctx.run("sgdisk -og {0}".format(device))
    # refresh os table partition information
    ctx.run("partprobe {0}".format(device))
    # setup partitions
    ctx.run(
        "sgdisk -n 1:2048:1026047 -c 1:\"EFI System Partition\" -t 1:ef00 {0}"
        .format(device)
    )
    ctx.run(
        "sgdisk -n 2:1026048:3074047 -c 2:\"Linux /boot\" -t 2:8300 {0}"
        .format(device)
    )
    # get end of disk and partition "/"
    end_sector = ctx.run("sgdisk -E {0}".format(device))
    lvm_partition = (
        "sgdisk -n 3:3074048:{0} -c 3:\"Linux LVM\" -t 3:8e00 {1}".format(
            end_sector.stdout.strip(),
            device
        )
    )
    print("lvm_partition: {0}".format(lvm_partition))
    ctx.run(lvm_partition)
    ctx.run("sgdisk -p {0}".format(device))


@task
def format_disk(ctx, device=None, group=None):
    """
    Format disk for full encryption and LVM volumes
    """
    # setup encryption
    ctx.run(
        "cryptsetup luksFormat -c aes-xts-plain64 -s 512 {0}3".format(device),
        pty=True
    )
    ctx.run("cryptsetup open {0}3 base".format(device), pty=True)
    # initialize LVM Volume
    ctx.run("pvcreate -f /dev/mapper/base")
    # create volume GROUP
    ctx.run("vgcreate {0} /dev/mapper/base".format(group))
    # creating LVM partitions
    ctx.run("lvcreate -L 50G -n root {0}".format(group))
    ctx.run("lvcreate -L 2G -C y -n swap {0}".format(group))
    ctx.run("lvcreate -l 100%FREE -n home {0}".format(group))
    # creating filesystem for partitions
    ctx.run("mkfs.ext4 -L archsystem /dev/mapper/{0}-root".format(group))
    ctx.run("mkfs.ext4 -L archuser /dev/mapper/{0}-home".format(group))
    ctx.run("mkswap /dev/mapper/{0}-swap".format(group))
    boot_disk = get_uuid(ctx, "{0}2".format(device))
    efi_boot_disk = get_uuid(ctx, "{0}1".format(device))
    ctx.run(
        "mkfs.ext4 -L boot {0}".format(boot_disk.stdout.strip()),
        pty=True,
        echo=True
    )
    ctx.run(
        "mkfs.fat -F32 {0}".format(efi_boot_disk.stdout.strip()),
        echo=True
    )


@task
def mount(ctx, src=None, dst=None):
    """
    Create mount point and moint source to destination
    """
    ctx.run("mkdir -p {0}".format(dst))
    ctx.run("mount {0} {1}".format(src, dst))


@task
def mount_volumes(ctx, device=None, group=None):
    """
    Mount volumes
    """
    boot_disk = get_uuid(ctx, "{0}2".format(device))
    efi_boot_disk = get_uuid(ctx, "{0}1".format(device))
    mount(ctx, "/dev/mapper/{0}-root".format(group), DEST)
    mount(ctx, boot_disk.stdout.strip(), "{0}/boot".format(DEST))
    mount(ctx, efi_boot_disk.stdout.strip(), "{0}/boot/efi".format(DEST))
    mount(ctx, "/dev/mapper/{0}-home".format(group), "{0}/home".format(DEST))


@task
def install_basic(ctx, group=None):
    """
    Install basic Linux system
    """
    # installing basic system
    ctx.run("pacstrap {0} base base-devel".format(DEST))
    # generating fstab
    ctx.run("genfstab -U -p {0} >> {0}/etc/fstab".format(DEST))
    # adding swap to fstab
    swap_fstab = "/dev/mapper/{0}-swap swap swap defaults 0 0".format(group)
    ctx.run("echo \"{0}\" >> {1}/etc/fstab".format(swap_fstab, DEST))


@task
def lvm_init(ctx):
    """
    Workaround to initiate logical volumes before chroot
    """
    ctx.run("mkdir {0}/hostrun".format(DEST))
    ctx.run("mount --bind /run {0}/hostrun".format(DEST))


@task
def launch_chroot(ctx, device=None, group=None, user=None):
    """
    chroot to the new system and run installation script
    """
    ctx.run("cp install.sh {0}/root".format(DEST))
    ctx.run(
        "arch-chroot {0} /bin/bash -c 'bash /root/install.sh && add -e system "
        "-d {1} -g {2} -u {3}'".format(
            DEST,
            device,
            group,
            user
        )
    )


@task
def unmount(ctx):
    """
    Unmount volumes
    """
    ctx.run("umount -l {0}/hostrun".format(DEST), warn=True)
    # umount all partitions
    ctx.run("umount -l -R {0}".format(DEST), warn=True)


@task
def lvm_mount(ctx):
    """
    mount lvm share
    """
    ctx.run("mkdir -p /run/lvm")
    ctx.run("mount --bind /hostrun/lvm /run/lvm")


@task
def set_locales(ctx, group=None):
    """
    Set system locales values
    """
    # setting timezone
    ctx.run("ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime")
    ctx.run("hwclock --systohc")
    # setting en_US locale
    ctx.run("sed -i \"s/#en_US.UTF-8/en_US.UTF-8/g\" /etc/locale.gen")
    # generating locales
    ctx.run("locale-gen")
    # setting hostname
    ctx.run("echo \"{0}\" > /etc/hostname".format(group))
    # adding hostname to hosts
    ctx.run(
        "echo \"127.0.1.1	{0}.localdomain {0}\" >> /etc/hosts".format(group)
    )


@task
def get_iethernet(ctx):
    """
    Get network interface
    """
    interfaces = ctx.run("ip addr")
    p = re.compile("^[0-9]: (en[^:]*):")
    for interface in interfaces.stdout.split("\n"):
        m = p.match(interface)
        if m:
            yield m.group(1)


@task
def setup_internet(ctx):
    """
    Setup internet for system
    """
    iethernet = get_iethernet(ctx)
    print("iethernet: {0}".format(iethernet))
    for interface in iethernet:
        ctx.run("systemctl enable dhcpcd@{0}".format(interface))


@task
def setup_gui(ctx):
    """
    Setup GUI for system
    """
    pkgs = [
        "nvidia",
        "nvidia-utils",
        "nvidia-settings",
        "lightdm-gtk-greeter",
        "xorg-server",
        "xorg-xinit",
        "xterm",
        "xorg-xclock",
        "awesome",
    ]
    packages(ctx, " ".join(pkgs))
    ctx.run("systemctl enable lightdm.service")
    change_awesome_font(ctx)


@task
def passwd(ctx, user="root"):
    """
    Set password for user
    """
    ctx.run("passwd {0}".format(user), echo_stdin=False, pty=True)


@task
def setup_pacman(ctx):
    ctx.run("echo '[multilib]' >> /etc/pacman.conf")
    ctx.run("echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf")


@task
def install_yaourt(ctx):
    ctx.run("echo '[archlinuxfr]' >> /etc/pacman.conf")
    ctx.run("echo 'SigLevel = Never' >> /etc/pacman.conf")
    ctx.run(
        "echo 'Server = http://repo.archlinux.fr/$arch'"
        " >> /etc/pacman.conf"
    )
    packages(ctx, "yaourt")


@task
def aur_packages(ctx, pkgs):
    ctx.run("yaourt --noconfirm -Syu {0}".format(pkgs))


@task
def setup_gaming(ctx):
    pkgs = [
        "lib32-nvidia-utils",
        "steam-native-runtime",
        "steam",
    ]
    setup_pacman(ctx)
    packages(ctx, ' '.join(pkgs))
    install_yaourt(ctx)
    aur_packages(ctx, "discord")


@task
def root_install(ctx):
    """
    Install root requirements
    """
    passwd(ctx)
    pkgs = [
        "grub",
        "efibootmgr",
        "sudo",
    ]
    packages(ctx, " ".join(pkgs))
    setup_internet(ctx)
    setup_gui(ctx)
    sound(ctx)


def install_firefox(ctx):
    pkgs = [
        "ttf-roboto",
        "libx264",
        "firefox",
        "firefox-ublock-origin",
        "firefox-extension-self-destructing-cookies",
        "firefox-extension-requestpolicy-continued",
        "firefox-flashgot",
    ]
    packages(ctx, " ".join(pkgs))


@task
def install(ctx, package):
    """
    Install package and call specific install if exist
    """
    if package == "firefox":
        install_firefox(ctx)


@task
def user_install(ctx, user=None):
    """
    Install user requirements
    """
    ctx.run(
        "useradd -m -g users -G adm,log,wheel -s /bin/bash {0}".format(user)
    )
    ctx.run("echo '%wheel      ALL=(ALL) ALL' | EDITOR='tee -a' visudo")
    passwd(ctx, user)
    setup_gaming(ctx)
    # TODO: packages collection with some default configurations
    pkgs = [
        "chromium",
        "termite",
        "git",
        "openssh",
        "atom",
        "vim",
        "zsh",
    ]
    install(ctx, "firefox")
    packages(ctx, " ".join(pkgs))


@task
def boot_install(ctx, device=None):
    """
    Install boot requirements regarding system
    """
    root_disk = ctx.run(
        "find -L /dev/disk/by-uuid -samefile {0}3".format(device),
        echo=True
    )
    # configuration of grub for encryption on top of lvm / volume
    ctx.run(
        "echo 'GRUB_CMDLINE_LINUX=\"cryptdevice={0}:base\"'"
        " >> /etc/default/grub".format(root_disk.stdout.strip()),
        echo=True
    )
    ctx.run(
        "echo 'GRUB_ENABLE_CRYPTODISK=y' >> /etc/default/grub",
        echo=True
    )
    # add hooks to mkinitcpio
    hooks = (
        "base udev autodetect keyboard keymap modconf block "
        "encrypt lvm2 filesystems fsck"
    )
    ctx.run(
        "echo 'HOOKS=\"{0}\"' >> /etc/mkinitcpio.conf".format(hooks),
        echo=True
    )
    ctx.run("mkinitcpio -p linux", warn=True)
    ctx.run("grub-mkconfig -o /boot/grub/grub.cfg")
    ctx.run("mkdir -p /boot/efi/EFI")
    ctx.run(
        "grub-install --recheck --target=x86_64-efi "
        "--efi-directory=/boot/efi --bootloader-id=arch_grub",
        echo=True
    )


@task
def lvm_unmount(ctx):
    """
    Unmount lvm trick
    """
    ctx.run("umount -l /run/lvm", warn=True)


@task
def arch(ctx, device=None, group=None, user=None):
    """
    Install archlinux
    """
    test_connection(ctx)
    partition_disk(ctx, device)
    format_disk(ctx, device, group)
    mount_volumes(ctx, device, group)
    install_basic(ctx, group)
    lvm_init(ctx)
    launch_chroot(ctx, device, group, user)
    unmount(ctx)
    print("install done")


@task
def system(ctx, device=None, group=None, user=None):
    """
    Install minimum system configuration
    """
    lvm_mount(ctx)
    set_locales(ctx, group)
    root_install(ctx)
    user_install(ctx, user)
    boot_install(ctx, device)
    lvm_unmount(ctx)
    print("finish with chroot")


@task
def test(ctx):
    ctx.run("echo 'it works!'")
    print(ctx.config['packages'])
