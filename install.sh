#!/usr/bin/env bash

set -e -x -u

device=$1
group=$2

sgdisk -Z ${device}
sgdisk -og ${device}
partprobe ${device}
sgdisk -n 1:2048:1026047 -c 1:"EFI System Partition" -t 1:ef00 ${device}
sgdisk -n 2:1026048:3074047 -c 2:"Linux /boot" -t 2:8300 ${device}
end=`sgdisk -E ${device}`
sgdisk -n 3:3074048:${end} -c 3:"Linux LVM" -t 3:8e00 ${device}

cryptsetup luksFormat -c aes-xts-plain64 -s 512 ${device}p3
cryptsetup open ${device}p3 base
pvcreate -f /dev/mapper/base
vgcreate ${group} /dev/mapper/base
lvcreate -L 50G -n root ${group}
lvcreate -L 2G -C y -n swap ${group}
lvcreate -l 100%FREE -n home ${group}

mkfs.ext4 -L nixsystem /dev/mapper/${group}-root
mkfs.ext4 -L maxter /dev/mapper/${group}-home
mkswap /dev/mapper/${group}-swap
efi_boot_uuid=`find -L /dev/disk/by-id -samefile ${device}p1`
boot_uuid=`find -L /dev/disk/by-id -samefile ${device}p2`
mkfs.ext4 -L boot /dev/disk/by-id/${boot_uuid}
mkfs.fat -F32 /dev/disk/by-id/${efi_boot_uuid}

mkdir -p /mnt
mount /dev/mapper/${group}-root /mnt

mkdir -p /mnt/boot
mount /dev/disk/by-id/${boot_uuid} /mnt/boot

mkdir -p /mnt/boot/efi
mount /dev/disk/by-id/${efi_boot_uuid} /mnt/boot/efi

swapon /dev/mapper/${group}-swap
