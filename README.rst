Add
###

Add is an installer which is able to make full OS install or user programs

----

Install
*******

You can install it with pip from git:

.. code-block:: bash

  curl -O https://gitlab.com/mvaude/add/raw/master/install.sh && bash install.sh

----

Usage
*****


To install arch just use it:

.. code-block:: bash

  add arch -d /dev/sda -g computer-name -u toto

If you need any help:

.. code-block:: bash

  add --help
  add arch --help

----

Configuration
*************

You can configure your installation copying and modifying `add.yml` to `~/.add.yml` or `/etc/add.yml`.
